from selenium import webdriver
import tkinter
from  tkinter import filedialog
from hidden_chrome import HiddenChromeWebDriver
import requests, os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from zipfile import ZipFile
import json
from xml.dom import minidom

TOKEN = ""

class Mod:
    def __init__(self, mod_object):
        self.object = mod_object
        if "localizations" in mod_object.keys():
            for entry in self.object["localizations"]:
                if entry["lang"]["code"] == "en":
                    self.name = entry["title"]
                    break
            self.version = self.object["versions"][0]["game_version"]["version"]
            self.download_link = self.object["versions"][0]["download_url"]
            self.state = "needs update"
            self.wg_link = "https://wgmods.net/"+str(self.object["id"])+"/"
        else:
            self.name = self.object["title"]
            self.version = self.object["version"]
            self.download_link = self.object["download_url"]
            self.state = self.object["state"]
            self.wg_link = self.object["wg_link"]

    def set_state(self, state):
        self.state = state
        
def init_browser():
    path_to_config = os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout")
    if not os.path.exists(os.path.join(path_to_config, "chromedriver.exe")):
        with requests.get("https://chromedriver.storage.googleapis.com/81.0.4044.69/chromedriver_win32.zip", stream=True) as rq:
            rq.raise_for_status()
            with open(os.path.join(path_to_config, "chromedriver.zip"), "wb+") as f:
                for chunk in rq.iter_content(chunk_size=1024):
                    f.write(chunk)
        with ZipFile(os.path.join(path_to_config, "chromedriver.zip"), "r") as ZO:
            ZO.extractall(path_to_config)
        os.remove(os.path.join(path_to_config, "chromedriver.zip"))

def get_csrf_token():
    global TOKEN
    init_browser()
    chromedriver_path=os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout", "chromedriver.exe")
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    driver = HiddenChromeWebDriver(chromedriver_path, options=options)
    driver.get("https://wgmods.net")
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "Header_logo--3heik")))
    token = None
    while token == None:
        token = driver.execute_script("return window.__SETTINGS__.csrfToken")
    driver.close()
    TOKEN = token
    return token

def add_mod(url, config):
    temp_mod = get_mod(url)
    if not any(temp_mod.name in temp["title"] for temp in config["mods"]):
        temp_obj = {
            "title": temp_mod.name,
            "version": temp_mod.version,
            "download_url": temp_mod.download_link,
            "state": temp_mod.state,
            "wg_link": temp_mod.wg_link
        }
        config["mods"].append(temp_obj)

def get_mod(url):
    global TOKEN
    mod_id = url.split('/')[-2:-1][0]
    url = f"https://wgmods.net/api/mods/{mod_id}/"
    return Mod(json.loads(requests.get(url, headers={"X-CSRFToken" : TOKEN, "X-Requested-With": "XMLHttpRequest"}).text))

def init():
    if not os.path.exists(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout")):
        os.mkdir(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout"))
    if not os.path.exists(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config")):
        print("Welcome to the WoT ModScout.\nThis seems to be your first time opening the script.\nPlease select your World of Tanks installation Directory (The Folder where the World_of_Tanks.exe is).")
        root = tkinter.Tk()
        root.withdraw()
        game_path = filedialog.askdirectory(title="Select the WoT Installation Directory", mustexist=True).strip()
        root.destroy()
        version = get_game_version(game_path)
        with open(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "w") as f:
            temp = {"game_path": game_path,"game_version": version, "config_path": os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "mods": []}
            f.write(json.dumps(temp))
    with open(os.path.join(os.getenv("LOCALAPPDATA"), "wot_modscout", "config"), "r") as f:
        return json.loads(f.read())

def get_game_version(game_path):
    elem = minidom.parse(os.path.join(game_path, "Version.xml"))
    version = elem.getElementsByTagName("version")[0].firstChild.nodeValue.strip()[2:10].strip()
    return version

def save_config(config):
    with open(config["config_path"], "w") as f:
        f.write(json.dumps(config))

def new_version(config):
    for entry in config["mods"]:
        entry["state"] = "needs update"
    save_config(config)

def download(url):
    with requests.get(url, stream=True) as req:
        req.raise_for_status()
        os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout","temp.zip")
        with open(os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout","temp.zip"), "wb") as f:
            for chunk in req.iter_content(chunk_size=1024):
                f.write(chunk)

def update_mods(list_of_mods, config):
    global TOKEN
    for entry in list_of_mods:
        if entry.state != "installed":
            online_mod = get_mod(entry.wg_link)
            if online_mod.version.strip() == config["game_version"]:
                entry.download_link = online_mod.download_link
                download(entry.download_link)
                unzip(config)
                for sub_entry in config["mods"]:
                    if sub_entry["title"] == entry.name:
                        sub_entry["state"] = "done"
                        sub_entry["version"] = online_mod.version
            else:
                for sub_entry in config["mods"]:
                    if sub_entry["title"] == entry.name:
                        sub_entry["state"] = "no update available"

def unzip(config):
    game_path = config["game_path"]
    print("extracting...")
    with ZipFile(os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout","temp.zip"), "r") as ZO:
        ZO.extractall(game_path)
    os.remove(os.path.join(os.getenv("LOCALAPPDATA"),"wot_modscout","temp.zip"))


