import os, sys, threading, wgmods, time

def browser_wrap():
    global TOKEN
    wgmods.init_browser()
    token = wgmods.get_csrf_token()
    TOKEN = token

def banner():
    return ("  __  __           _  _____                 _ \n"+
            " |  \/  |         | |/ ____|               | |\n"+
            " | \  / | ___   __| | (___   ___ ___  _   _| |_ \n"+
            " | |\/| |/ _ \ / _` |\___ \ / __/ _ \| | | | __|\n"+
            " | |  | | (_) | (_| |____) | (_| (_) | |_| | |_ \n"+
            " |_|  |_|\___/ \__,_|_____/ \___\___/ \__,_|\__|   Ver: 0.1\n")

def startup():
    browser_thread = threading.Thread(target=browser_wrap)
    browser_thread.start()
    config = wgmods.init()
    return config

def parse_answer():
    global config, TOKEN
    answer = input("\nModScout> ").strip()
    commands = ["add", "a", "help", "h", "quit", "q", "update", "u", "token", "t", "list", "l"]
    
    if answer == "":
        print("Wrong command. Use the command 'help' to display the help page")
        parse_answer()
        return
    
    answer_args = answer.split(" ")

    while answer_args[0] not in commands:
        print("Wrong command. Use the command 'help' to display the help page.")
        answer = input("\nModScout> ").strip()
    
    #add-Block
    if answer_args[0] == "add" or answer_args[0] == "a":
        if len(answer_args) == 2:
            if answer_args[1].startswith("http"):
                wgmods.add_mod(answer_args[1], config)
                answer_args[0] = "list"
            else:
                print("Wrong url format. Please Copy the complete url from wgmods.net: 'https://wgmods.net/4111/'")
                parse_answer()
                return
        else:
            print("Wrong usage of the command. Use the command 'help' to display the help page.")
            parse_answer()
            return

    #update-Block
    if answer_args[0] == "update" or answer_args[0] == "u":
        for entry in config["mods"]:
            if entry["state"] != "installed":
                online_mod = wgmods.get_mod(entry["wg_link"])
                if online_mod.version == config["game_version"]:
                    entry["download_url"] = online_mod.download_link
                    wgmods.download(entry["download_url"])
                    wgmods.unzip(config)
                    entry["version"] = online_mod.version
                    entry["state"] = "installed"
                else:
                    entry["state"] = "no update available"
        answer_args[0] = "list"
        wgmods.save_config(config)
                    

    #quit-Block
    if answer_args[0] == "quit" or answer_args[0] == "q":
        print("shutting down")
        wgmods.save_config(config)
        sys.exit()

    #list-Block
    if answer_args[0] == "list" or answer_args[0] == "l":
        print(f"Managed mods:\n")
        mod_objects = []
        for entry in config["mods"]:
            mod = wgmods.Mod(entry)
            mod_objects.append(mod)
            print(f"  {mod.name}  |  {mod.state}")
        return

    #token-Block
    if answer_args[0] == "token" or answer_args[0] == "t":
        print(f"Token: {TOKEN}")
        return
    
    #help-Block
    if answer_args[0] == "help" or answer_args[0] == "h":
        os.system("cls")
        print(banner())
        print("'add' / 'a'     | adding a new mod to manage")
        print("                | this command accepts only wgmods.net links (with https:// at the beginning)")
        print("                | example: 'add https://wgmods.net/4111'\n")
        print("'list' / 'l'    | list all managed mods")
        print("                | example: 'list'\n")
        print("'update' / 'u'  | updating all of the mods that need an update")
        print("                | example: 'update'\n")
        print("'token' / 't'   | displaying the authentification token used to access the wgmods api")
        print("                | example: 'token'\n")
        print("'quit' / 'q'    | closing the program")
        print("                | example: 'quit'\n")
        print("'help' / 'h'    | displaying this help page")
        print("                | example: 'help\n")
        return

####GLOBALS####
TOKEN = ""
###############

os.system("cls")

config = startup()

print(banner())
print("loading token...", end="\r")
while TOKEN == "":
    time.sleep(.1)
print("checking mod updates...", end= "\r")
if config["game_version"] != wgmods.get_game_version(config["game_path"]):
    wgmods.new_version(config)
# check mods and compare to online variant

os.system("cls")
print(banner())

if len(config["mods"]) == 0:
    print("No mods to manage")
else:
    print(f"Managed mods:\n")
    mod_objects = []
    for entry in config["mods"]:
        mod = wgmods.Mod(entry)
        mod_objects.append(mod)
        print(f"  {mod.name}  |  {mod.state}")

try:
    while True:
        parse_answer()
except KeyboardInterrupt:
    print("\nShut down forced by user")
    print("Saving config")
    wgmods.save_config(config)
    print("Shutting down")
    sys.exit()



